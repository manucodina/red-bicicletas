const mapaBicis = L.map('mapa_bicis').setView([-34.6126031, -58.4811748], 15);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>'
}).addTo(mapaBicis);


$.ajax({
    dataType: 'json',
    url:'/api/bicicletas',
    success: (result) => {
        console.log(result);
        result.bicicletas.forEach((bici) => {
            L.marker(bici.ubicacion, {title: bici.id}).addTo(mapaBicis);
        })
    }
})