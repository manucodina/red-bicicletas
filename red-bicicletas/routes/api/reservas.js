const express = require('express');
const router = express.Router();
const reservaController = require('../../controllers/api/reservaControllerAPI');

router.get('/', reservaController.reserva_list);
router.post('/update', reservaController.actualizar_reserva);

module.exports = router;