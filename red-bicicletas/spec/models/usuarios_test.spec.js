const mongoose = require('mongoose');
const Reserva = require('../../models/reserva');
const Bicicleta = require('../../models/bicicleta');
const Usuario = require('../../models/usuario');


describe('Testing Usarios ', function() {
    
    beforeAll(function(done){
        
        let mongoDB = "mongodb://localhost/testdb";
        mongoose.connect(mongoDB, { useNewUrlParser:true });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
            console.log('We are connected to test database');
            done();
        });
    });

    afterEach(function(done){
        Reserva.deleteMany({}, function(err, success) {
            if (err) console.log(err);
            Usuario.deleteMany({}, function( err, success ){
                if (err) console.log(err);
                Bicicleta.deleteMany({}, function(err, success)  {
                    if (err) console.log(err);
                    done();
                });
            });
        });
    });

    describe('Cuando un usuario reserva una bici', () => {
        it('debe de existir la reserva', (done) => {
            
            let usuario = new Usuario({ nombre: 'Richard' });
            usuario.save();
            console.log(' usuario ' + usuario);

            let bicicleta = new Bicicleta({ code: 1, color: "verde", modelo: "urbana" });
            bicicleta.save();
            console.log('bicicleta ' + bicicleta);

            let hoy = new Date();
            let manana = new Date();
            manana.setDate(hoy.getDate() + 1);

            usuario.reservar(bicicleta.id, hoy, manana, ( err, reserva ) => {
                Reserva.find({}).populate('bicicleta').populate('usuario').exec(function(err, reservas) {
                    console.log(reservas[0]);
                    console.log(reservas[0].usuario);
                    expect(reservas.length).toBe(1);
                    expect(reservas[0].diasDeReserva()).toBe(2);
                    expect(reservas[0].bicicleta.code).toBe(1);
                    expect(reservas[0].usuario.nombre).toBe(usuario.nombre);
                    done();
                });
            });
        });
    });
    
});