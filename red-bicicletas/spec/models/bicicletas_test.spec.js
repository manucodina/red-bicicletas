const mongoose = require('mongoose');
const Bicicleta = require('../../models/bicicleta');


describe('Test Bicicletas', () => {
    beforeAll((done) => {
        const mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection to test Data Base error'));
        db.once('open', () => {
            console.log('We are connected to test Data Base');
            done();
        });
    });

    afterEach((done) => {
        Bicicleta.deleteMany({}, (err, success) => {
            if (err) console.log(err);
            done();
        })
    });

    describe('Bicicleta.createInstance', () => {
        it('crea instancia de una bicicleta', () => {
            let bici = Bicicleta.createInstance(1, 'rojo', 'montaña', [-34.6126031, -58.4811748]);
            expect(bici.code).toBe(1);
            expect(bici.color).toBe('rojo');
            expect(bici.modelo).toBe('montaña');
            expect(bici.ubicacion[0]).toEqual(-34.6126031);
            expect(bici.ubicacion[1]).toEqual(-58.4811748);
        })
    })

    describe('Bicicleta.allBicis', () => {
        it('empieza vacía', (done) => {
            Bicicleta.allBicis(function(err, bicis){
                if (err) console.log(err);
                expect(bicis.length).toBe(0)
                done();
            })
        })
    })
    describe('Bicicleta.add', () => {
        it('agrega solo una bici', (done) => {
            let aBici = new Bicicleta({code: 1, color: 'rojo', modelo: 'urbana'});
            Bicicleta.add(aBici, function(err, newBici){
                if (err) console.log(err);
                Bicicleta.allBicis(function(err, bicis){
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);
                    done();
                })
            })
        })
    })

    describe('Bicicleta.findByCode', () => {
        it('debe devolver la bici con code 1', (done) => {
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);

                let aBici = new Bicicleta({code: 1, color: "verde", modelo: "urbana"});
                Bicicleta.add(aBici, function(err, newBici){
                    if (err) console.log(err);

                    let aBici2 = new Bicicleta({code: 2, color: "roja", modelo: "urbana"});
                    Bicicleta.add(aBici2, function(err, newBici){
                        if (err) console.log(err);
                        Bicicleta.findByCode(1, function (error, targetBici){
                            expect(targetBici.code).toBe(aBici.code);
                            expect(targetBici.color).toBe(aBici.color);
                            expect(targetBici.modelo).toBe(aBici.modelo);

                            done();
                        });
                    });
                });
            });
        });
    });
})
/*const Bicicleta = require('../../models/bicicleta');

beforeEach(() => { Bicicleta.allBicis = []; })

describe('Bicicleta.allBicis', () => {
    it('comienza vacia', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
    })
});

describe('Bicicletas.add', () => {
    it('agregamos una', () => {
        expect(Bicicleta.allBicis.length).toBe(0);

        let a = new Bicicleta(1, 'rojo', 'urbana', [-34.6126031, -58.4811748]);
        Bicicleta.add(a)

        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a)
    })
})

describe('Bicicleta.findById', () => {
    it('debe devolver la bici con id 1', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
        let aBici = new Bicicleta(1, 'rojo', 'urbana', [-34.6126031, -58.4811748]);
        let aBici2 = new Bicicleta(2, 'azul', 'montaña', [-34.6126031, -58.4811748]);
        Bicicleta.add(aBici);
        Bicicleta.add(aBici2);

        let targetBici = Bicicleta.findById(1);
        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(aBici.color);
        expect(targetBici.modelo).toBe(aBici.modelo);

    })
})

describe('Bicicleta.removeById', () => {
    it('debe devolver 0', () => {
        let aBici = new Bicicleta(1, 'rojo', 'urbana', [-34.6126031, -58.4811748]);
        Bicicleta.add(aBici);
        Bicicleta.removeById(1);
        expect(Bicicleta.allBicis.length).toBe(0);
    })
})*/