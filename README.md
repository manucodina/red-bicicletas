Para poder hacer funcionar la API en modo de desarrollo y hacer correr el documento HTML:
1. Dirigite al directorio de red-bicicletas/red-bicicletas.
2. Ingresá el comando npm install para que se instalen las dependencias ya que el node_modules no está incluido en el repositorio GIT.
3. ingresá "npm run devstart".